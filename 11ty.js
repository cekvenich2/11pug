const fs = require('fs')
module.exports = (e) => {
	e.setWatchThrottleWaitTime(1000)
	e.setServerOptions({
		module: '@11ty/eleventy-server-browsersync',

		reloadDelay: 50,

		port: 8080,
		// Change the default weinre port
		ui: {
			port: 8081,
			weinre: {
				port: 8082
			}
		},
		open: 'ui',
		reloadOnRestart: true,
		injectChanges: false,

		baseDir: 'public',
		domDiff: false,
		directory: false,
		ghostMode: true,
		
		logConnections: true,
		
	})

	e.setPugOptions({ pretty: false })

	e.addPassthroughCopy('Xsrc/assets')
	e.addPassthroughCopy('src/**/*.map')
	e.addPassthroughCopy('src/**/*.woff2')
	e.addPassthroughCopy('src/**/*.js')
	e.addPassthroughCopy('src/**/*.json')
	e.addPassthroughCopy('src/**/*.css')
	e.addPassthroughCopy('src/**/*.xml')
	e.addPassthroughCopy('src/**/*.jpg')
	e.addPassthroughCopy('src/**/*.ico')
	e.addPassthroughCopy('src/**/*.png')
	e.addPassthroughCopy('src/**/*.webp')
	e.addPassthroughCopy('src/**/*.gif')
	e.addPassthroughCopy('src/**/*.mp4')
	e.addPassthroughCopy('src/**/*.svg')


	e.ignores.add('public')

	// data section:  /////////////////////////////////////////
	// http://benmyers.dev/blog/eleventy-data-cascade

	e.addCollection('blogs', function (collection) {
		// write json in /blog folder of subfolder front matter
		const col = collection.getFilteredByGlob('./src/blog/**/*.pug')
		let ret = []
		for (item of col) {
			for (i in item) {
				if (i != 'data') continue
				let data = item[i]
				let hit = {}
				for (j in data) {
					//console.log('2~'+j)
					if (j == 'pkg') continue
					if (j == 'eleventy') continue
					if (j == 'collections') continue
					if (j == 'layout') continue
					if (j == 'page') {
						let pg = data[j]
						hit['pg_date'] = pg['date']
						hit['time'] = new Date(pg['date']).getTime()
						hit['pg_url'] = pg['url']
						continue
					}
					hit[j] = data[j]
				} // in 2 data
				ret.push(hit)
			} // in 1 item
		} // outer
		// sort by path length
		ret.sort((a, b) => (a.pg_url.length > b.pg_url.length ? 1 : -1))
		//remove first, as that is root
		ret.shift()
		// sort by time
		ret.sort((a, b) => (a.time > b.time ? -1 : 1))
		// now write it so we can use it
		setTimeout(() => {
			// delay write so public dir is created on first buil
			fs.writeFile(
				'./public/blog/_generated.json',
				JSON.stringify(ret, null, 2),
				(err) => {
					if (err) console.error(err)
					else console.info('_generated json')
				},
			)
		}, 150)
		return ret
	})

	// done w/ config
	return e
}
