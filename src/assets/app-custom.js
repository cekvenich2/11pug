
// loaded from footer

depp.require(['log', 'vitals'], function () {

	log.setDefaultLevel('trace')

	log.debug(document.readyState)

	webVitals.onCLS(log.debug, { reportAllChanges: true })
	webVitals.onFID(log.debug)
	webVitals.onLCP(log.debug)
	webVitals.onINP(log.debug)
	webVitals.onFCP(log.debug)
	webVitals.onTTFB(log.debug)

	let burger = document.querySelector('.burger');
	let navbar = document.querySelector('.navbar-menu');
	burger.addEventListener('click', () => {
		log.info('burg')
		burger.classList.toggle('is-active');
		navbar.classList.toggle('is-active');
	})

}) // req

